opam-version: "2.0"
name: "refinedc"
synopsis: "RefinedC verification framework"
description: """
RefinedC is a framework for verifying idiomatic, low-level C code using a
combination of refinement types and ownership types.
"""
license: "BSD-3-Clause"

maintainer: ["Michael Sammler <msammler@mpi-sws.org>"
             "Rodolphe Lepigre <lepigre@mpi-sws.org>"]
authors: ["Michael Sammler" "Rodolphe Lepigre" "Kayvan Memarian"]

homepage: "https://plv.mpi-sws.org/refinedc"
bug-reports: "https://gitlab.mpi-sws.org/iris/refinedc/issues"
dev-repo: "git+https://gitlab.mpi-sws.org/iris/refinedc.git"

depends: [
  "coq-lithium" {= version | = "~dev"}
  "cerberus" {= "~dev"}
  "cmdliner" {= "1.0.4"}
  "sexplib0" {= "v0.14.0"} # see https://github.com/rems-project/cerberus/issues/194
  "earley" {= "3.0.0"}
  "toml" {= "5.0.0"}
  "ubase" {= "0.04"}
]

build: [
  [make "prepare-install-refinedc"]
  ["dune" "subst"] {pinned}
  ["dune" "build" "-p" name "-j" jobs]
]
url { src: "git+https://gitlab.mpi-sws.org/iris/refinedc.git#bf22d5ee10cabb80734a3a27884efe5cdac44868" }
