opam-version: "2.0"
maintainer: "Ralf Jung <jung@mpi-sws.org>"
authors: "The Iris Team"
license: "BSD-3-Clause"
homepage: "https://iris-project.org/"
bug-reports: "https://gitlab.mpi-sws.org/iris/iris/issues"
dev-repo: "git+https://gitlab.mpi-sws.org/iris/iris.git"
version: "dev"

synopsis: "The canonical example language for Iris"
description: """
This package defines HeapLang, a concurrent lambda calculus with references, and
uses Iris to build a program logic for HeapLang programs.
"""
tags: [
  "logpath:iris.heap_lang"
]

depends: [
  "coq-iris" {= version}
]

build: ["./make-package" "iris_heap_lang" "-j%{jobs}%"]
install: ["./make-package" "iris_heap_lang" "install"]
url { src: "git+https://gitlab.mpi-sws.org/iris/iris.git#ba65caa39f217bec71b7a09a23d0421073fd455d" }
